import cn.hutool.core.util.ArrayUtil;
import org.junit.Assert;
import org.junit.Test;
import priv.xakml.Main;
import priv.xakml.fileassistant.entity.DbHelper;
import priv.xakml.fileassistant.entity.Dir;
import priv.xakml.fileassistant.entity.file;
import sun.security.util.Length;

import javax.swing.text.html.parser.Entity;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqliteTest {


    public SqliteTest() throws SQLException, ClassNotFoundException {


    }

    //region Test methods
//    @Test
//    public void testCreateSqliteDbFile() throws SQLException, ClassNotFoundException {
//        Connection  connection = getConnection("file-index.db");
//        createDirTable(connection);
//        createSubDirTable(connection);
//        createFileTable(connection);
//    }


    @Test
    public void testTableExists() throws SQLException, ClassNotFoundException {
        String table_name = "Dir";
        boolean isExists = new DbHelper().tableExists(table_name);
        Assert.assertEquals(true, isExists);
    }


//    @Test
//    public void testInsertData() throws SQLException, ClassNotFoundException {
//        Connection  connection = getConnection("file-index.db");
//        Statement statement = connection.createStatement();
//        PreparedStatement pst = connection.prepareStatement("insert  into Dir(dir_path,dir_path_hash,files_count,sub_dir_count) values (?,?,?,?)" );
//        pst.setString(1, "path");
//        pst.setString(2, "hash");
//        pst.setInt(3, 0);
//        pst.setInt(4, 0);
//        int isOK = pst.executeUpdate();
//        System.out.println(isOK);
//        connection.close();
//    }

    @Test
    public void testSaveDirObject() throws SQLException, ClassNotFoundException {
        priv.xakml.fileassistant.entity.Dir dir = new Dir();
        dir.setDirPath("/volumes/photos");
        dir.setDirPath("/volumes");
        dir.setDirPathHash("sha1-md5");
        dir.setFilesCount(20);
        dir.setSubDirCount(10);
        if(new DbHelper().saveDir(dir)){
            System.out.println(String.format("new dir id = %s",dir.getId()));
        }
    }

    @Test
    public void testSaveFile() throws SQLException, ClassNotFoundException {
        file file = new file();
        file.setFolder_id(1);
        file.setFile_name("1.jpg");
        file.setFile_path("/volumes/photos/1.jpg");
        file.setFile_size(100);
        file.setMd5("MD5");
        file.setSha1("sha1");
        new DbHelper().saveFile(file);
    }
    //endregion

    @Test
    public void testHuTool()
    {
        List<File> files = cn.hutool.core.io.FileUtil.loopFiles(new File("D:\\文件备份\\DISK-250G\\DCIM"));
        for (File f : files){
            System.out.println(f.getAbsolutePath());
        }
    }

    @Test
    public void testLoopDirectories(){
        File file = new File("D:\\文件备份\\DISK-250G\\DCIM");
       List<File> directories =  loopDirectories(file);
       for (File dir : directories){
           System.out.println(dir.getAbsolutePath());
       }
    }

    public List<File> loopDirectories(File file){
        final List<File> fileList = new ArrayList<>();
        if (null == file || false == file.exists()) {
            return fileList;
        }

        if (file.isDirectory()) {
            fileList.add(file);
            final File[] subFiles = file.listFiles();
            if (ArrayUtil.isNotEmpty(subFiles)) {
                for (File tmp : subFiles) {
                    fileList.addAll(loopDirectories(tmp));
                }
            }
        }

        return fileList;
    }

    @Test
    public void testBackChar(){
        int length = 483;
        String length_str = String.format("%d", length);
        System.out.println(length_str.length());
        String format_string = "hello%"+length_str.length() +"d/%-3d";
        String output =  String.format(format_string, 20,length);
        System.out.println(output);
    }
    public void printProgress(int current,int total){
        String total_str = String.format("%d",total);
        int total_str_length = total_str.length();
    }
}

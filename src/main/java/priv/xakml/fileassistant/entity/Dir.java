package priv.xakml.fileassistant.entity;

public class Dir {
    int id;
    String dirPath;
    String dirPathHash;
    int filesCount;
    int subDirCount;

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 设置目录路径
     * @param dirPath 目录路径
     */
    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }

    public void setDirPathHash(String dirPathHash) {
        this.dirPathHash = dirPathHash;
    }

    public void setFilesCount(int filesCount) {
        this.filesCount = filesCount;
    }

    public void setSubDirCount(int subDirCount) {
        this.subDirCount = subDirCount;
    }

    public int getId() {
        return id;
    }

    public String getDirPath() {
        return dirPath;
    }

    public String getDirPathHash() {
        return dirPathHash;
    }

    public int getFilesCount() {
        return filesCount;
    }

    public int getSubDirCount() {
        return subDirCount;
    }
}

package priv.xakml.fileassistant.entity;

/**
 * 子目录
 */
public class SubDir {
    int id;
    String subDir_Path;
    String subDir_Path_hash;
    int files_count;
    int photos_count;
    int homeDirId;
    public SubDir() {

    }

    /**
     * 初始化子目录
     * @param id
     */
    public SubDir(int id) {
        this.id = id;
    }

    /**
     * 初始化子目录
     * @param id
     * @param subDir_Path
     */
    public SubDir(int id, String subDir_Path) {
        this.id = id;
        this.subDir_Path = subDir_Path;
    }

    /**
     * 初始化子目录
     * @param id
     * @param subDir_Path
     * @param subDir_Path_hash
     */
    public SubDir(int id, String subDir_Path, String subDir_Path_hash) {
        this.id = id;
        this.subDir_Path = subDir_Path;
        this.subDir_Path_hash = subDir_Path_hash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubDir_Path() {
        return subDir_Path;
    }

    public void setSubDir_Path(String subDir_Path) {
        this.subDir_Path = subDir_Path;
    }

    public String getSubDir_Path_hash() {
        return subDir_Path_hash;
    }

    public void setSubDir_Path_hash(String subDir_Path_hash) {
        this.subDir_Path_hash = subDir_Path_hash;
    }

    public int getFiles_count() {
        return files_count;
    }

    public void setFiles_count(int files_count) {
        this.files_count = files_count;
    }

    public int getPhotos_count() {
        return photos_count;
    }

    public void setPhotos_count(int photos_count) {
        this.photos_count = photos_count;
    }

    public int getHomeDirId() {
        return homeDirId;
    }

    public void setHomeDirId(int homeDirId) {
        this.homeDirId = homeDirId;
    }
}

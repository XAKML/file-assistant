package priv.xakml.fileassistant.entity;

/**
 * 需要被索引的文件对象
 */
public class file {
    int folder_id;
    String file_name;
    String file_path;
    String md5;
    String sha1;
    long file_size;

    public long getFile_size() {
        return file_size;
    }

    public void setFile_size(long file_size) {
        this.file_size = file_size;
    }

    public file(){

    }
    /**
     * 初始化被索引文件类型
     * @param file_full_name 被索引文件的全路径
     */
    public file(String file_full_name){
        String sep = System.getProperty("file.separator");
        int index = file_full_name.lastIndexOf(sep);
        this.file_path = file_full_name.substring(0,index);
        this.file_name = file_full_name.substring(index,file_full_name.length() - index);
    }

    /**
     * 初始化被索引文件类型
     * @param file_full_name 被索引文件的全路径
     * @param md5_hex_value 被索引文件的哈希值（md5算法）
     * @param sha1_hex_value 被索引文件的哈希值（sha1算法）
     */
    public file(String file_full_name,String md5_hex_value,String sha1_hex_value){
        this(file_full_name);
        this.md5 = md5_hex_value;
        this.sha1 = sha1_hex_value;
    }

    /**
     * 所属目录Id（外键， 引用sub_dir表中的主键）
     * @return
     */
    public int getFolder_id() {
        return folder_id;
    }

    public void setFolder_id(int folder_id) {
        this.folder_id = folder_id;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }
}
